# SRB2 Cherry
[![latest release](https://badgen.net/static/get/cherry/pink)](https://mb.srb2.org/addons/redberries.8043/)
[![latest release](https://badgen.net/static/get/reveries/yellow)](https://codeberg.org/lavla/cherry/releases/latest)

SRB2 Cherry is a project containing the Reveries and Redberries source modifications of Sonic Robo Blast 2.

[Sonic Robo Blast 2](https://srb2.org/) is a 3D Sonic the Hedgehog fangame based on a modified version of [Doom Legacy](http://doomlegacy.sourceforge.net/).

## Dependencies
- libpng
- zlib
- libsdl2
- libsdl2_mixer
- libgme
- libcurl
- libopenmpt
- libminiupnpc

## Compiling

See [SRB2 Wiki/Source code compiling/makefiles](http://wiki.srb2.org/wiki/Source_code_compiling/Makefiles)

## Disclaimer
Sonic Team Junior is in no way affiliated with SEGA or Sonic Team. We do not claim ownership of any of SEGA's intellectual property used in SRB2.
